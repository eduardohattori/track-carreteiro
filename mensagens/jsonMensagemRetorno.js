/**
 * [exports description]
 * @author Eduardo Cortes
 * @date   2015-08-19
 * @param  {[type]}   app [description]
 * @return {[type]}       [description]
 */
module.exports = function(app) {
	var msg = function(devMsg, userMsg, errorCode, moreInfo) {
		var msg = {
			"developerMessage": devMsg,
			"userMenssage": userMsg,
			"errorCode": errorCode,
			"moreInfo": moreInfo
		};
		return msg;
	};
	return msg;
};