/**
 * [mongoose description]
 * @type {[type]}
 */
var Promise = require("bluebird");
var mongoose = Promise.promisifyAll(require('mongoose'));
var single_connection;
var env_url = {
    "test": "mongodb://localhost/grupo_test",
    "development": "mongodb://localhost/grupo"
  };

module.exports = function() {  
  var url = env_url[process.env.NODE_ENV];
  
  if(!single_connection) {
	  single_connection = mongoose.connect(url);
	}
  
  return single_connection;
};
