/**
 * [exports description]
 * @author Eduardo Cortes
 * @date   2015-08-24
 * @param  {[type]}   app [description]
 * @return {[type]}       [description]
 */
module.exports = function(app) {
	
	var connection = null;
	var entidade = null;
	var post = {
		
		getConnection: function() {
			return connection;
		},


		setConnection: function(conn) {
			connection = conn;
		},

		getEntidade: function() {
			return entidade;
		},

		setEntidade: function(ent) {
			entidade = ent;
		},
	}
	return post;
};