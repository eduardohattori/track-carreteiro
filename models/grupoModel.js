/**
 * [exports description]
 * @author Eduardo Cortes
 * @date   2015-08-19
 * @param  {[type]}   app [description]
 * @return {[type]}       [description]
 */
module.exports = function(app) {

  var db = require('../libs/mongoConnect')();
  //var autoIncrement = require('mongoose-auto-increment');
  //autoIncrement.initialize(db);
  var Schema = require('mongoose').Schema;

 /* var Settings = new Schema({
    nextSeqNumber: { type: Number, default: 1 }
  });*/

  var linhas = Schema({
    id: Number,
    nome: {type: String, required: true}
  });

  var filiais = Schema({id: Number,cd: Number});

  var grupo = Schema({
      id: Number
    , nome: { type: String, required: true, index: {unique: true} }
    , descricao: { type: String, required: true}
    , dataInclusao: {type: Date, required: true}
    , dataAlteracao: {type: Date, required: true}
    , ativo: {type: Boolean, required: true}
    , linhas: [linhas]
    , filiais: [filiais]
  });

  //grupo.plugin(autoIncrement.plugin, { model: 'grupo', field: 'id' });
  
                                             

  return db.model('grupo', grupo);
};