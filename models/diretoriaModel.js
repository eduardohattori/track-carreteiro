/**
 * [exports description]
 * @author Eduardo Cortes
 * @date   2015-08-19
 * @param  {[type]}   app [description]
 * @return {[type]}       [description]
 */
module.exports = function(app) {
	var Promise = require("bluebird");
	var diretoria = {
		adicionar: function(post) {
			return new Promise(function(resolve, reject) {
				var entidade = post.getEntidade();
				post.getConnection().execute(
					"INSERT INTO MAG_T_GM_CAD_DIRETORIA (ID_DIRETORIA, NOME, DESCRICAO, DT_INCLUSAO, DT_ALTERACAO, ST_ATIVO) VALUES (:ID_DIRETORIA, :NOME, :DESCRICAO, :DT_INCLUSAO, :DT_ALTERACAO, :ST_ATIVO)", {
						ID_DIRETORIA: {
							val: entidade.ID_DIRETORIA
						},
						NOME: {
							val: entidade.NOME
						},
						DESCRICAO: {
							val: entidade.DESCRICAO
						},
						DT_INCLUSAO: {
							val: entidade.DT_INCLUSAO
						},
						DT_ALTERACAO: {
							val: entidade.DT_ALTERACAO
						},
						ST_ATIVO: {
							val: entidade.ST_ATIVO
						}
					},
					function(err, result) {
						if (err) {
							console.error(err.message);
							reject(err);
						} else {
							resolve(result.rows);
						}
					});
			});
		},

		editar: function(post) {
			return new Promise(function(resolve, reject) {
				var entidade = post.getEntidade();
				connection.execute(
					"UPDATE MAG_T_GM_CAD_DIRETORIA  SET NOME = :NOME, DESCRICAO= :DESCRICAO, ST_ATIVO= :ST_ATIVO, DT_ALTERACAO=SYSDATE WHERE ID_DIRETORIA = :ID_DIRETORIA", {
						ID_DIRETORIA: {
							val: entidade.ID_DIRETORIA
						},
						NOME: {
							val: entidade.NOME
						},
						DESCRICAO: {
							val: entidade.DESCRICAO
						},
						ST_ATIVO: {
							val: entidade.ST_ATIVO
						}
					},
					function(err, result) {
						if (err) {
							console.error(err.message);
							reject(err);
						} else {
							resolve(result.rowsAffected);
						}
					});
			});
		},

		getAll: function(post) {
			return new Promise(function(resolve, reject) {

				post.getConnection().execute(
					"SELECT * FROM MAG_T_GM_CAD_DIRETORIA",
					function(err, result) {
						if (err) {
							console.error(err.message);
							reject(err);
						} else {
							resolve(result.rows);
						}
					});
			});

		},
		nextId: function(post) {
			return new Promise(function(resolve, reject) {

				post.getConnection().execute(
					"SELECT MAG_SQ_GM_DIRETORIA.NEXTVAL FROM DUAL",
					function(err, result) {
						if (err) {
							console.error(err.message);
							reject(err);
						} else {
							resolve(result.rows);
						}
					});
			});

		},

		remover: function(post) {
			return new Promise(function(resolve, reject) {
				post.getConnection().execute(
					"DELETE FROM MAG_T_GM_CAD_DIRETORIA WHERE ID_DIRETORIA = :ID_DIRETORIA", {
						ID_DIRETORIA: {
							val: post.getEntidade().ID_DIRETORIA
						}
					},
					function(err, result) {
						if (err) {
							console.error(err.message);
							reject(err);
						} else {
							resolve(result.rowsAffected);
						}
					});
			});
		}
	};
	return diretoria;
};