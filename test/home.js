/**
 * [app description]
 * @type {[type]}
 */
var app = require('../app'),
	should = require('should'),
	request = require('supertest')(app);

describe('No controller home', function() {
	it('deve retornar erro Not Found status 404 ao fazer GET /', function(done) {
		request.get('/')
			.end(function(err, res) {
				res.status.should.eql(404);

				done();
			});
	});

	it('deve retornar status 200 ao fazer GET /grupofilial', function(done) {
		request.get('/grupofilial')
			.end(function(err, res) {
				console.log(res.body[0].id);
				res.status.should.eql(200);
				done();
			});
	});

	it('deve retornar status 200 ao fazer GET /grupofilial/249', function(done) {
		request.get('/grupofilial/249')
			.end(function(err, res) {
				res.status.should.eql(200);
				done();
			});
	});

	it('deve retornar status 200 ao fazer GET /grupofilial/249/filiais', function(done) {
		request.get('/grupofilial/249/filiais')
			.end(function(err, res) {
				res.status.should.eql(200);
				done();
			});
	});

	it('deve retornar status 200 ao fazer GET /grupofilial/249/linhas', function(done) {
		request.get('/grupofilial/249/linhas')
			.end(function(err, res) {
				res.status.should.eql(200);
				done();
			});
	});

	it('deve retornar status 200 ao fazer GET /grupofilial?nome=teste', function(done) {
		request.get('/grupofilial?nome=teste')
			.end(function(err, res) {
				res.status.should.eql(200);
				done();
			});
	});

	it('deve retornar status 200 ao fazer GET com caractere invalido /grupofilial?nome=^', function(done) {
		request.get('/grupofilial?nome=^')
			.end(function(err, res) {
				res.status.should.eql(200);
				done();
			});
	});

	it('deve retornar status 404 ao fazer DELETE /grupofilial/249', function(done) {
		request.delete('/grupofilial/249')
			.end(function(err, res) {
				res.status.should.eql(404);
				done();
			});
	});

	it('deve retornar status 200 ao fazer PUT /grupofilial/278', function(done) {
			var grupo = {"nome": "Diretoria Teste " + new Date(),
						 "descricao": "Descricao da diretoria Teste",
						 "ativo": true,
						 "filiais": [{"id":1, "cd":5}],
						 "linhas": [{"id":1,"nome":"BRINQUEDOS"}]
						};

			request.put('/grupofilial/278')
			.send(grupo)
			.end(function(err, res) {
				res.status.should.eql(200);
				done();
			});
	});

	it('deve retornar status 500 ao fazer POST de registro duplicado em /grupofilial', function(done) {
		var grupo	 =	{"nome": "Diretoria Teste Edu3",
	 					 "descricao": "Descricao da diretoria Teste",
	 					 "ativo": true,
	 					 "filiais": [{"id":1, "cd":5}],
	 					 "linhas": [{"id":1,"nome":"BRINQUEDOS"}]
						};
			request.post('/grupofilial')
			.send(grupo)
			.end(function(err, res) {
				res.status.should.eql(500);
				done();
			});
	});

	it('deve retornar status 200 ao fazer POST de registro que não existe em /grupofilial', function(done) {
			var grupo	 =	{"nome": "Diretoria Teste Edu" + new Date(),
		 					 "descricao": "Descricao da diretoria Teste",
		 					 "ativo": true,
		 					 "filiais": [{"id":1, "cd":5}],
		 					 "linhas": [{"id":1,"nome":"BRINQUEDOS"}]
							};
				request.post('/grupofilial')
				.send(grupo)
				.end(function(err, res) {
					res.status.should.eql(200);
					done();
				});
		});

}); // Fim do describe