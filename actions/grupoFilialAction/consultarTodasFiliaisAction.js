/**
 * [exports description]
 * @author Eduardo Cortes
 * @date   2015-08-19
 * @param  {[type]}   app [description]
 * @return {[type]}       [description]
 */
module.exports = function(app) {

	var grupoModel = app.models.grupoModel;

	/**
	 * [execute description]
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   req [description]
	 * @param  {[type]}   res [description]
	 * @return {[type]}       [description]
	 */
	var execute = function(req, res) {

		RegExp.escape = function(s) {
			return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
		};

		var query = {};

		var queryNome = function(nome) {
			var reNome = new RegExp(nome, 'i');
			var query = {
				'nome': {
					$regex: reNome
				}
			};
			return query;
		};

		var queryStatus = function(status) {
			var query = {
				'ativo': status
			};
			return query;
		};

		var queryNomeEDescricao = function(nome, descricao) {
			var reDesc = new RegExp(descricao, 'i');
			var reNome = new RegExp(nome, 'i');

			var query = {
				'nome': {
					$regex: reNome
				},
				'descricao': {
					$regex: reDesc
				}
			};
			return query;
		};

		var queryDescricao = function(descricao) {
			var reDesc = new RegExp(descricao, 'i');
			var query = {
				'descricao': {
					$regex: reDesc
				}
			};
			return query;
		};

		if (req.query.nome) {
			query = queryNome(RegExp.escape(req.query.nome));
			if (req.query.descricao) {
				query = queryNomeEDescricao(RegExp.escape(req.query.nome), RegExp.escape(req.query.descricao));
			}

		} else if (req.query.descricao) {
			query = queryDescricao(RegExp.escape(req.query.descricao));

		} else if (req.query.ativo) {
			query = queryStatus(req.query.ativo);
		}

		grupoModel.find(query).exec(function(err, grupo) {
			res.status(200).send(grupo);
		});
	};
	return execute;
};