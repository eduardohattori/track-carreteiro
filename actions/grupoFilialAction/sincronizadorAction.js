/**
 * [exports description]
 * @author Eduardo Hattori
 * @date   2015-08-28
 * @param  {[type]}   app [description]
 * @return {[type]}       [description]
 */
 module.exports = function(app) {
 	
 	var Promise 	  		= require("bluebird");
	var oracledb 	  		= Promise.promisifyAll(require("oracledb"));
	var oracleConnect 		= app.libs.oracleConnect;
	var grupoModel 	  		= app.models.grupoModel;
	var oracleDBPost  		= app.models.oracleDBPost;
	var jsonMensagemRetorno = app.mensagens.jsonMensagemRetorno;
	var messageBundle 		= app.resources.messageBundle;
	var diretoriaModel 		= app.models.diretoriaModel;
	var linhaModel 			= app.models.linhaModel;

 	/**
	 * Processo Main
	 * @author Eduardo Hattori
	 * @date   2015-08-28
	 * @param  {[type]}   request  [description]
	 * @param  {[type]}   response [description]
	 * @return {[type]}            [description]
	 */
 	var execute = function(req, res){

 		var connection; 		 		

 		grupoModel.find().exec(function(err, grupos) {
 			if(grupos.length == 0){
 				oracledb.getConnection(oracleConnect.stringConexao, callBackConsultaTodos);
 			}
 			
		});


 	/**
	 * callBackConsultaTodos 
	 * @author Eduardo Hattori
	 * @date   2015-08-28
	 * @param  {[type]}   err  [description]
	 * @param  {[type]}   conn [description]
	 * @return {[type]}        [description]
	 */
	var callBackConsultaTodos= function(err, conn){
		 	connection = conn;
		 	oracleDBPost.setConnection(conn);
			diretoriaModel.getAll(oracleDBPost).then(incluirDiretoriaMongo).catch(function(err){console.log(err);});
	};

	/**
	 * incluirDiretoriaMongo 
	 * @author Eduardo Hattori
	 * @date   2015-08-28	
	 * @param  {[type]} grupoFiliais [description]
	 * @return {[type]}     		 [description]
	 */
	var incluirDiretoriaMongo = function(grupoFiliais){

		for(var i in grupoFiliais){
			var grupo = getNovoGrupoModel(grupoFiliais[i]);
			oracleDBPost.setEntidade(grupoFiliais[i][0]);
			grupo.linhas = linhaModel.getLinhaDiretoria(oracleDBPost).then(function(linhas){ return linhas;});
			grupo.filiais = new Array();
			grupo.save(callbackSave).then(commit);
		}
	};

	/**
	 * callbackSave 
	 * @author Eduardo Hattori
	 * @date   2015-08-28	
	 * @param  {[type]} err   [description]
	 * @param  {[type]} grupo [description]
	 * @return {[type]}       [description]
	 */
	function callbackSave(err, grupo) {
		if (err) {
			console.log(err);

			connection.release(function(err) {
				if (err) {
					console.log(err);
				}
			});
		}

	}

	/**
	 * commit 
	 * @author Eduardo Hattori
	 * @date   2015-08-28	
	 * @param  {[type]} grupoFiliais [description]
	 * @return {[type]}     		 [description]
	 */
	function commit(grupo) {
		connection.commit(callBackCommit);
		res.status(200).send(jsonMensagemRetorno(messageBundle.registroIncluido, messageBundle.registroIncluido, 200, ""));
	}

	/**
	 * getNovoGrupoModel 
	 * @author Eduardo Hattori
	 * @date   2015-08-28	
	 * @param  {[type]} entity [description]
	 * @return {[type]}     		 [description]
	 */
 	var getNovoGrupoModel = function(entity) {
			return new grupoModel({
				id: entity[0],
				nome: entity[1],
				descricao: entity[2],
				dataInclusao: entity[3],
				dataAlteracao: entity[4],
				linhas: new Array(),
				filiais: new Array(),
				ativo: entity[5]
			});
		}
 	};

 	return execute;
 };

