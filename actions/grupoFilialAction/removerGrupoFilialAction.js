/**
 * [exports description]
 * @author Eduardo Cortes
 * @date   2015-08-19
 * @param  {[type]}   app [description]
 * @return {[type]}       [description]
 */
module.exports = function(app) {

	var grupoModel = app.models.grupoModel;
	var Promise = require("bluebird");
	var oracledb = Promise.promisifyAll(require("oracledb"));
	var oracleConnect = app.libs.oracleConnect;
	var oracleDBPost = app.models.oracleDBPost;
	var messageBundle = app.resources.messageBundle;
	var jsonMensagemRetorno = app.mensagens.jsonMensagemRetorno;
	var diretoriaModel = app.models.diretoriaModel;

	var connection;
	var grupo;
	var res;
	var req;

	/**
	 * Processo Main
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   req [description]
	 * @param  {[type]}   res [description]
	 * @return {[type]}       [description]
	 */
	var execute = function(request, response) {
		
		res = response;
		req = request;

		grupoModel.findOne({
			'id': req.params.id
		}, callBackFindOne);
	};


	/**
	 * [callBackFindOne description]
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   err        [description]
	 * @param  {[type]}   collection [description]
	 * @return {[type]}              [description]
	 */
	var callBackFindOne = function(err, collection) {

		if (err) {
			res.status(500).send(jsonMensagemRetorno(messageBundle.registroNaoRemovido, err.message, err.code, ""));
		} else {

		grupo = collection;
		if (collection) {
			oracledb.getConnection(oracleConnect.stringConexao, callBackConnection);

		} else {
			console.log("nao encontrou");
			res.status(404).send(jsonMensagemRetorno(messageBundle.grupoFilialNaoEncontrado, "", 0, ""));
		}
		}
	};

	/**
	 * [callBackConnection description]
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   err  [description]
	 * @param  {[type]}   conn [description]
	 * @return {[type]}        [description]
	 */
	var callBackConnection = function(err, conn) {
		connection = conn;
		if (err) {
			console.log('Erro ao connectar no oracle');
			res.status(500).send(jsonMensagemRetorno(messageBundle.registroNaoRemovido, err.message, err.code, ""));
		} else {

			oracleDBPost.setConnection(connection);
			oracleDBPost.setEntidade({
				ID_DIRETORIA: parseInt(grupo.id)
			});

			diretoriaModel.remover(oracleDBPost).then(removerNoMongoDb).catch(naoRemoveuRegistro);
		}
	};

	/**
	 * [removerNoMongoDb description]
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   msgs [description]
	 * @return {[type]}        [description]
	 */
	var removerNoMongoDb = function(msgs) {
		grupo.remove().then(commit);
	};

	/**
	 * [naoRemoveuRegistro description]
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   err [description]
	 * @return {[type]}       [description]
	 */
	var naoRemoveuRegistro = function(err) {
		if (err) {
			connection.release(function(err) {
				if (err) {
					console.log(err);
				}
			});
			res.status(500).send(jsonMensagemRetorno(messageBundle.registroNaoRemovido, err.message, 500, ""));
		}
	};

	/**
	 * [commit description]
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   grupo [description]
	 * @return {[type]}         [description]
	 */
	var commit = function(grupo) {
		connection.commit(callBackCommit);
	};

	/**
	 * [callBackCommit description]
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   err [description]
	 * @return {[type]}       [description]
	 */
	var callBackCommit = function(err) {
		if (err) {
			connection.release(function(err) {
				if (err) {
					console.log(err);
				}
			});
			res.status(500).send(jsonMensagemRetorno(messageBundle.registroNaoRemovido, err.message, err.code, ""));
		} else {
			res.status(200).send(jsonMensagemRetorno(messageBundle.registroRemovido, messageBundle.registroRemovido, 200, ""));
		}
	};

	return execute;
};