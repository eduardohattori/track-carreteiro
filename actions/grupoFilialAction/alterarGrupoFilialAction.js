/**
 * [exports description]
 * @author Eduardo Cortes
 * @date   2015-08-19
 * @param  {[type]}   app [description]
 * @return {[type]}       [description]
 */
module.exports = function(app) {

	var Promise = require("bluebird");
	var oracledb = Promise.promisifyAll(require("oracledb"));
	var oracleConnect = app.libs.oracleConnect;
	var grupoModel = app.models.grupoModel;
	var oracleDBPost = app.models.oracleDBPost;
	var diretoriaModel = app.models.diretoriaModel;
	var jsonMensagemRetorno = app.mensagens.jsonMensagemRetorno;
	var messageBundle = app.resources.messageBundle;

	var res;
	var req;
	var grupo;
	/**
	 * [execute description]
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   req [description]
	 * @param  {[type]}   res [description]
	 * @return {[type]}       [description]
	 */
	var execute = function(request, response) {
		res = response;
		req = request;
		grupoModel.findOne({
			'id': req.params.id
		}, callBackFindOne);
	};
	/**
	 * [callBackFindOne description]
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   err   [description]
	 * @param  {[type]}   grupo [description]
	 * @return {[type]}         [description]
	 */
	var callBackFindOne = function(err, collection) {
		if (err) {
			console.log(err);
			res.status(500).send(jsonMensagemRetorno(messageBundle.grupoFilialNaoAtualizado, err.message, err.code, ""));
		} else {


			grupo = collection;
			if (grupo) {
				grupo.nome = req.body.nome;
				grupo.descricao = req.body.descricao;
				grupo.dataAlteracao = new Date();
				grupo.ativo = req.body.ativo;

				oracledb.getConnection(oracleConnect.stringConexao, callBackConnection);

			} else {
				res.status(404).send(jsonMensagemRetorno(messageBundle.grupoFilialNaoEncontrado, "", 0, ""));
			}
		}
	};

	/**
	 * [callBackConnection description]
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   err  [description]
	 * @param  {[type]}   conn [description]
	 * @return {[type]}        [description]
	 */
	var callBackConnection = function(err, conn) {
		console.log("callBackConnection");
		connection = conn;
		if (err) {
			console.log('Erro ao connectar no oracle');
			res.status(500).send(jsonMensagemRetorno(messageBundle.grupoFilialNaoAtualizado, err.message, err.code, ""));
		} else {

			oracleDBPost.setConnection(connection);
			oracleDBPost.setEntidade({
				ID_DIRETORIA: grupo.id,
				NOME: grupo.nome,
				DESCRICAO: grupo.descricao,
				DT_ALTERACAO: new Date(),
				ST_ATIVO: grupo.ativo ? 1 : 0
			});

			diretoriaModel.editar(oracleDBPost).then(editarNoMongoDb).catch(naoEditouRegistro);
		}
	};

	/**
	 * [editarNoMongoDb description]
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   msgs [description]
	 * @return {[type]}        [description]
	 */
	var editarNoMongoDb = function(msgs) {
		grupo.save(callbackSave).then(commit);
	};

	/**
	 * Callback do Save realizado no mOngdb
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   err   [description]
	 * @param  {[type]}   grupo [description]
	 * @return {[type]}         [description]
	 */
	function callbackSave(err, grupo) {
		if (err) {
			console.log(err);

			connection.release(function(err) {
				if (err) {
					console.log(err);
				}
			});

			res.status(500).send(jsonMensagemRetorno(messageBundle.grupoFilialNaoAtualizado, err.message, 500, ""));

		}

	}

	/**
	 * [naoEditouRegistro description]
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   err [description]
	 * @return {[type]}       [description]
	 */
	var naoEditouRegistro = function(err) {
		if (err) {
			connection.release(function(err) {
				if (err) {
					console.log(err);
				}
			});
			res.status(500).send(jsonMensagemRetorno(messageBundle.grupoFilialNaoAtualizado, err.message, 500, ""));
		}
	};

	/**
	 * [commit description]
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   grupo [description]
	 * @return {[type]}         [description]
	 */
	var commit = function(grupo) {
		connection.commit(callBackCommit);
	};

	/**
	 * [callBackCommit description]
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   err [description]
	 * @return {[type]}       [description]
	 */
	var callBackCommit = function(err) {
		if (err) {
			connection.release(function(err) {
				if (err) {
					console.log(err);
				}
			});
			res.status(500).send(jsonMensagemRetorno(messageBundle.grupoFilialNaoAtualizado, err.message, err.code, ""));
		} else {
			res.status(200).send(jsonMensagemRetorno(messageBundle.registroAtualizado, messageBundle.registroRemovido, 200, ""));
		}
	};

	return execute;
};