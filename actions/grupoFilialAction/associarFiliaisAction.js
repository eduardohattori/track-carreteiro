/**
 * Adciona filais em grupoFilial 
 * @author Eduardo Cortes
 * @date   2015-08-19
 * @param  {Object}   app [Objeto aplicação]
 * @return {Objecto}       [Objeto aplicação com a action embutida]
 */
module.exports = function(app) {

	var grupoModel = app.models.grupoModel;
	var jsonMensagemRetorno = app.mensagens.jsonMensagemRetorno;
	var messageBundle = app.resources.messageBundle;

	var request;
	var response;
	/**
	 * Processo Main
	 * O sistema realiza uma busca do grupofilial pelo id
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {Object}   req [Requisição realizada pelo HTTP]
	 * @param  {Object}   res [Resposta para o HTTP]
	 * @return {addFiliaisEmGrupoFilialAction}       [Action responsável por adicionar filias em um grupoFilial]
	 */
	var execute = function(req, res) {
		request = req;
		response = res;

		grupoModel.findOne({
			'id': request.params.id
		}, callbackFindOne);
	};


	/**
	 * Função Callback responsável por invocar o método SAVE
	 * passando como parãmetro a requisição com a coleção de filiais a serem incluídas
	 * no grupofilial
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {Error}   err   [Objeto Error retornado do processamento do método findOne]
	 * @param  {JsonCollection}   grupoFilial [description]
	 * @return {Nothing}         [Nothing]
	 */
	var callbackFindOne = function(err, grupoFilial) {
		if (err) {
			response.status(500).send(jsonMensagemRetorno(messageBundle.naoFoiPossivelIncluirFiliais, err.message, err.code, ""));
		}

		if (grupoFilial) {
			grupoFilial.filiais = request.body;
			grupoFilial.save(callBackSave);
			response.status(200).send(jsonMensagemRetorno(messageBundle.filiaisAssociadasComSucesso, "", 0, ""));
		} else {
			response.status(404).send(jsonMensagemRetorno(messageBundle.grupoFilialNaoEncontrado, "", 0, ""));
		}
	};

	/**
	 * Função callback responsável por validar o processo de salvamento das filiais
	 * e invocar a remoção das filiais existentes e inclusão das novas filiais
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {Error}   err   [Objeto Error retornado do processamento do método findOne]
	 * @param  {JsonCollection}   grupoFilial [description]
	 * @return {Nothing}         [Nothing]
	 */
	var callBackSave = function(err, grupoFilial) {
		if (err) {
			response.status(500).send(jsonMensagemRetorno(messageBundle.naoFoiPossivelAtualizarFiliais, err.message, err.code, ""));
		}
	};



	return execute;
};