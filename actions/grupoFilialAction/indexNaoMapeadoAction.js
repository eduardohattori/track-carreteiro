/**
 * [exports description]
 * @author Eduardo Cortes
 * @date   2015-08-19
 * @param  {[type]}   app [description]
 * @return {[type]}       [description]
 */
module.exports = function(app) {

	var jsonMensagemRetorno = app.mensagens.jsonMensagemRetorno;
	var messageBundle = app.resources.messageBundle;

	/**
	 * Processo Main
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   req [description]
	 * @param  {[type]}   res [description]
	 * @return {[type]}       [description]
	 */
	var execute = function(req, res) {
		res.status(404).send(jsonMensagemRetorno(messageBundle.rotaNaoMapeada, messageBundle.acessarGrupoFilial, 0, ""));
	};
	return execute;
};