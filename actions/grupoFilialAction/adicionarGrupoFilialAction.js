/**
 * [exports description]
 * @author Eduardo Cortes
 * @date   2015-08-19
 * @param  {[type]}   app [description]
 * @return {[type]}       [description]
 */
module.exports = function(app) {

	var Promise = require("bluebird");
	var oracledb = Promise.promisifyAll(require("oracledb"));
	var oracleConnect = app.libs.oracleConnect;
	var grupoModel = app.models.grupoModel;
	var oracleDBPost = app.models.oracleDBPost;
	var jsonMensagemRetorno = app.mensagens.jsonMensagemRetorno;
	var messageBundle = app.resources.messageBundle;
	var diretoriaModel = app.models.diretoriaModel;
	

	/**
	 * Processo Main
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   request  [description]
	 * @param  {[type]}   response [description]
	 * @return {[type]}            [description]
	 */
	var execute = function(req, res) {
	
		var connection;
		oracledb.getConnection(oracleConnect.stringConexao, callBackConnection);

		/**
		 * Cria uma instancia de GrupoFilial com as requisições enviadas via HTTP
		 * @author Eduardo Cortes
		 * @date   2015-08-24
		 * @param  {[type]}   req [description]
		 * @return {[type]}       [description]
		 */
		var getNovoGrupoModel = function(req) {
			return new grupoModel({
				nome: req.body.nome,
				descricao: req.body.descricao,
				dataInclusao: new Date(),
				dataAlteracao: new Date(),
				linhas: req.body.linhas,
				filiais: req.body.filiais,
				ativo: req.body.ativo
			});
		}

		/**
		 * Calback da conexão
		 * @author Eduardo Cortes
		 * @date   2015-08-24
		 * @param  {[type]}   err  [description]
		 * @param  {[type]}   conn [description]
		 * @return {[type]}        [description]
		 */
		function callBackConnection(err, conn) {
			connection = conn;
			if (err) {
				console.log('Erro ao connectar no oracle:' + err);
				res.status(500).send(jsonMensagemRetorno(messageBundle.registroNaoIncluido, err.message, err.code, ""));
			} else {

				oracleDBPost.setConnection(connection);
				oracleDBPost.setEntidade({
					ID_DIRETORIA: 0,
					NOME: req.body.nome,
					DESCRICAO: req.body.descricao,
					DT_INCLUSAO: new Date(),
					DT_ALTERACAO: new Date(),
					ST_ATIVO: req.body.ativo ? 1 : 0
				});

				diretoriaModel.nextId(oracleDBPost).then(inserirGrupoFilialNoOracle).catch(naoIncluiuRegistro);

			}
		}

		/**
		 * Promise catch da captura do Sequence Id
		 * @author Eduardo Cortes
		 * @date   2015-08-24
		 * @param  {[type]}   err [description]
		 * @return {[type]}       [description]
		 */
		function naoIncluiuRegistro(err) {
			if (err) {
				res.status(500).send(jsonMensagemRetorno(messageBundle.registroNaoIncluido, err.message, 500, ""));
			}
		}

		/**
		 * Realiza a inclusão de um novo GrupoFilial na base de dados do oracle
		 * @author Eduardo Cortes
		 * @date   2015-08-24
		 * @param  {[type]}   id [description]
		 * @return {[type]}      [description]
		 */
		function inserirGrupoFilialNoOracle(id) {
			oracleDBPost.getEntidade().ID_DIRETORIA = id[0][0];
			diretoriaModel.adicionar(oracleDBPost).then(inserirGrupoFilialNoMongo).catch(callBackCommit);
		}

		/**
		 * Realiza a inclusão de um novo Grupofilial no Mongodb
		 * @author Eduardo Cortes
		 * @date   2015-08-24
		 * @return {[type]}   [description]
		 */
		function inserirGrupoFilialNoMongo() {
			var grupo = getNovoGrupoModel(req);
			grupo.id = oracleDBPost.getEntidade().ID_DIRETORIA;
			grupo.save(callbackSave).then(commit);
		}

		/**
		 * Callback do Save realizado no mOngdb
		 * @author Eduardo Cortes
		 * @date   2015-08-24
		 * @param  {[type]}   err   [description]
		 * @param  {[type]}   grupo [description]
		 * @return {[type]}         [description]
		 */
		function callbackSave(err, grupo) {
			if (err) {
				console.log(err);

				connection.release(function(err) {
					if (err) {
						console.log(err);
					}
				});

				res.status(500).send(jsonMensagemRetorno(messageBundle.registroNaoIncluido, err.message, 500, ""));

			}

		}

		/**
		 * Commit da operação realizada no banco oracle
		 * @author Eduardo Cortes
		 * @date   2015-08-24
		 * @param  {[type]}   grupo [description]
		 * @return {[type]}         [description]
		 */
		function commit(grupo) {
			connection.commit(callBackCommit);
			res.status(200).send(jsonMensagemRetorno(messageBundle.registroIncluido, messageBundle.registroIncluido, 200, ""));
		}

		/**
		 * Calback do commit realizado no oracle
		 * @author Eduardo Cortes
		 * @date   2015-08-24
		 * @param  {[type]}   err [description]
		 * @return {[type]}       [description]
		 */
		function callBackCommit(err) {
			if (err) {
				connection.release(function(err) {
					if (err) {
						console.log(err);
					}
				});
				res.status(500).send(jsonMensagemRetorno(messageBundle.registroNaoIncluido, err.message, err.code, ""));
			}
		}

	};

	return execute;
};