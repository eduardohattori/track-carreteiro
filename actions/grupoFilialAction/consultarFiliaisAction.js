/**
 * [exports description]
 * @author Eduardo Cortes
 * @date   2015-08-19
 * @param  {[type]}   app [description]
 * @return {[type]}       [description]
 */
module.exports = function(app) {

	var grupoModel = app.models.grupoModel;

	var res;
	/**
	 * Modulo Main
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   req [description]
	 * @param  {[type]}   res [description]
	 * @return {[type]}       [description]
	 */
	var execute = function(req, response) {
		res = response;
		grupoModel.findOne({
			'id': req.params.id
		}, 'filiais', callbackFindOne);
	};

	/**
	 * Callback do findOne
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   err   [description]
	 * @param  {[type]}   grupo [description]
	 * @return {[type]}         [description]
	 */
	var callbackFindOne = function(err, grupo) {
		if (err) {
			res.status(500).send(jsonMensagemRetorno(err.message, err.message, err.code, ""));
		}

		if (grupo) {
			res.status(200).json(grupo.filiais);
		} else {
			res.status(200).json([]);
		}

	};
	return execute;
};