/**
 * [exports description]
 * @author Eduardo Cortes
 * @date   2015-08-19
 * @param  {[type]}   app [description]
 * @return {[type]}       [description]
 */
module.exports = function(app) {

	var grupoModel = app.models.grupoModel;
	var jsonMensagemRetorno = app.mensagens.jsonMensagemRetorno;
	var messageBundle = app.resources.messageBundle;

	var request;
	var response;
	/**
	 * [execute description]
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   req [description]
	 * @param  {[type]}   res [description]
	 * @return {[type]}       [description]
	 */
	var execute = function(req, res) {
		request = req;
		response = res;
		grupoModel.findOne({
			'id': request.params.id
		}, callbackFindOne);
	};

	/**
	 * [callbackFindOne description]
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   err   [description]
	 * @param  {[type]}   grupo [description]
	 * @return {[type]}         [description]
	 */
	var callbackFindOne = function(err, grupo) {
		if (err) {
			response.status(500).send(jsonMensagemRetorno(messageBundle.naoFoiPossivelIncluirLinhas, err.message, err.code, ""));
		}

		if (grupo) {

			grupo.linhas = request.body;
			grupo.save(callBackSave);

			response.status(200).send(jsonMensagemRetorno(messageBundle.linhasAssociadasComSucesso, "", 0, ""));
		} else {
			response.status(404).send(jsonMensagemRetorno(messageBundle.grupoFilialNaoEncontrado, "", 0, ""));
		}
	};

	/**
	 * [callBackSave description]
	 * @author Eduardo Cortes
	 * @date   2015-08-24
	 * @param  {[type]}   err   [description]
	 * @param  {[type]}   grupo [description]
	 * @return {[type]}         [description]
	 */
	var callBackSave = function(err, grupo) {
		if (err) {
			response.status(500).send(jsonMensagemRetorno(messageBundle.naoFoiPossivelAtualizarLinhas, err.message, err.code, ""));
			return err;
		}
	};


	return execute;
};