/**
 * Mapeamento de rotas da API
 * @author Eduardo Cortes
 * @date   2015-08-19
 * @param  {[type]}   app [description]
 * @return {[type]}       [description]
 */
module.exports = function(app) {
  var grupoController = app.controllers.grupoController;

  app.get('/', grupoController.indexNaoMapeado);
  app.get('/grupofilial', grupoController.consultarTodasFiliais);

  app.get('/sincronizador/', grupoController.sincronizador);

  app.get('/grupofilial/:id', grupoController.consultarGrupoFilialPorID);
  app.get('/grupofilial/:id/filiais', grupoController.consultarFiliais);
  app.get('/grupofilial/:id/linhas', grupoController.consultarLinhas);

  app.post('/grupofilial', grupoController.adicionarGrupoFilial);

  app.put('/grupofilial/:id', grupoController.alterarGrupoFilial);

  app.put('/grupofilial/:id/filiais', grupoController.associarFiliais);
  app.put('/grupofilial/:id/linhas', grupoController.associarLinhas);

  app.delete('/grupofilial/:id', grupoController.removerGrupoFilial);

};