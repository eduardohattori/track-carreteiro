/**
 * [express description]
 * @type {[type]}
 */
var express = require('express'),
	load = require('express-load'),
	bodyParser = require('body-parser'),
	morgan = require('morgan'),
	app = express();

var port = 8084;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

//app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));
app.use(morgan('combined'))

load('models')
	.then('libs')
	.then('mensagens')
	.then('resources')
	.then('middlewares')
	.then('actions')
	.then('controllers')
	.then('routes')
	.into(app);

var error = app.middlewares.error;

app.use(error.notFound);
app.use(error.serverError);

app.listen(port, function() {
	console.log("API Grupo Filial working in port:" + port);
});

module.exports = app;