/**
 * [exports description]
 * @author Eduardo Cortes
 * @date   2015-08-19
 * @param  {[type]}   app [description]
 * @return {[type]}       [description]
 */
module.exports = function(app) {

	var msg = app.mensagens.jsonMensagemRetorno;
	var resources = app.resources.messageBundle;

	var errors = {
		notFound: function(req, res, next) {
			res.status(404).send(msg(resources.rotaNaoMapeada, resources.acessarGrupoFilial, 0, ""));
		},

		serverError: function(error, req, res, next) {
			res.status(500).send(msg(error.message, "", error.code, error.message));
		}
	};
	return errors;
};