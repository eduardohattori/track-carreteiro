/**
 * Bundle de Mensagens
 * @author Eduardo Cortes
 * @date   2015-08-19
 * @param  {[type]}   app [description]
 * @return {[type]}       [description]
 */
module.exports = function(app){
	
	var mensagens = {
		//NEGATIVAS
		naoFoiPossivelIncluirFiliais : function(){return 'Não foi possível incluir as filiais';}(),
		naoFoiPossivelAtualizarFiliais : function(){return 'Não foi possível atualizar as filiais';}(),
		naoFoiPossivelIncluirLinhas : function(){return 'Não foi possível incluir as linhas';}(),
		naoFoiPossivelAtualizarLinhas : function(){return 'Não foi possível atualizar as linhas';}(),
		registroNaoIncluido : function(){return 'Registro não incluído';}(),
		registroNaoRemovido : function(){return 'Registro não removido';}(),
		rotaNaoMapeada: function() {return 'Rota não mapeada';}(),
		grupoFilialNaoEncontrado : function(){return 'Grupo filial não encontrado';}(),
		grupoFilialNaoAtualizado : function(){return 'Grupo filial não atualizado';}(),

		
		//POSITIVAS
		filiaisAssociadasComSucesso : function(){return 'Filiais associadas com sucesso';}(),
		linhasAssociadasComSucesso : function(){return 'Linhas associadas com sucesso';}(),

		registroRemovido : function(){return 'Registro removido com sucesso';}(),
		registroIncluido : function(){return 'Registro incluído com sucesso';}(),
		registroAtualizado : function(){return 'Registro atualiza com sucesso';}(),

		//INFORMATIVAS		
		acessarGrupoFilial: function() {return 'Por favor acesse /grupofilial para utilização da API';}(),
		
	};

	return mensagens;
};