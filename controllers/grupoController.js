/**
 * [exports description]
 * @author Eduardo Cortes
 * @date   2015-08-19
 * @param  {[type]}   app [description]
 * @return {[type]}       [description]
 */
module.exports = function(app) {

	var associarFiliaisAction = app.actions.grupoFilialAction.associarFiliaisAction;
	var associarLinhasAction = app.actions.grupoFilialAction.associarLinhasAction;
	var removerGrupoFilialAction = app.actions.grupoFilialAction.removerGrupoFilialAction;
	var alterarGrupoFilialAction = app.actions.grupoFilialAction.alterarGrupoFilialAction;
	var adicionarGrupoFilialAction = app.actions.grupoFilialAction.adicionarGrupoFilialAction;
	var consultarLinhasAction = app.actions.grupoFilialAction.consultarLinhasAction;
	var consultarFiliaisAction = app.actions.grupoFilialAction.consultarFiliaisAction;
	var consultarTodasFiliaisAction = app.actions.grupoFilialAction.consultarTodasFiliaisAction;
	var consultarGrupoFilialPorIDAction = app.actions.grupoFilialAction.consultarGrupoFilialPorIDAction;
	var indexNaoMapeadoAction = app.actions.grupoFilialAction.indexNaoMapeadoAction;
	var sincronizadorAction = app.actions.grupoFilialAction.sincronizadorAction;

	var GrupoController = {
		indexNaoMapeado: function(req, res) {
			indexNaoMapeadoAction(req, res);
		},

		consultarGrupoFilialPorID: function(req, res) {
			consultarGrupoFilialPorIDAction(req, res);
		},

		consultarTodasFiliais: function(req, res) {
			consultarTodasFiliaisAction(req, res);
		},

		consultarFiliais: function(req, res) {
			consultarFiliaisAction(req, res);
		},

		consultarLinhas: function(req, res) {
			consultarLinhasAction(req, res);
		},

		adicionarGrupoFilial: function(req, res) {
			adicionarGrupoFilialAction(req, res);
		},

		alterarGrupoFilial: function(req, res) {
			alterarGrupoFilialAction(req, res);
		},

		removerGrupoFilial: function(req, res) {
			removerGrupoFilialAction(req, res);
		},

		associarLinhas: function(req, res) {
			associarLinhasAction(req, res);
		},

		associarFiliais: function(req, res) {
			associarFiliaisAction(req, res);
		},

		sincronizador: function(req, res){
			sincronizadorAction(req,res);
		}
	};

	return GrupoController;
};